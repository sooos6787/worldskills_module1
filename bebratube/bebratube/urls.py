from unicodedata import name
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from main.views import (
    index,
    SignUpView,
    VideoCreateView,
    userVideos,
    video,
    set_like,
    set_dislike,
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('users/', include('django.contrib.auth.urls'), name='login'),
    path('signup/', SignUpView.as_view(), name='signup'),
    path('myvideo/', userVideos, name='user_videos'),
    path('upload/', VideoCreateView.as_view(), name="upload_video"),
    path('video/<int:id>', video, name="video"),
    path('', index, name='home'),
    path('video/<int:id>/like', set_like, name='like'),
    path('video/<int:id>/dislike', set_dislike, name='dislike'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

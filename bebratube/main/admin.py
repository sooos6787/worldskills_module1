from django.contrib import admin
from . models import (
    Video,
    Like,
    Comment,
)
admin.site.register([
    Video,
    Like,
    Comment,
])

from django.urls import reverse
from django.db import models


class Video(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название ролика")
    describtion = models.TextField(verbose_name="Описание ролика", blank=True)
    videfile = models.FileField(verbose_name="Файл видео")
    created_datetime = models.DateTimeField(auto_now_add=True)

    GAME_CATEGORY = "Game"
    FILM_CATEGORY = "Film"
    CATS_CATEGORY = "Cats"
    category = models.CharField(max_length=10,
                                verbose_name="Категория ролика",
                                choices=[
                                    (GAME_CATEGORY, "Игры"),
                                    (FILM_CATEGORY, "Фильмы"),
                                    (CATS_CATEGORY, "Коты"),
                                ],)
    NO_RESTRICTION = "NO"
    VIOLATION_RESTRICTION = "VIOLATION"
    SHADOW_BAN_RESTRICTION = "SHADOW"
    BAN_RESTRICTION = "BAN"
    restriction = models.CharField(max_length=10,
                                   verbose_name="Ограничение",
                                   choices=[
                                       (NO_RESTRICTION, "Нет ограничений"),
                                       (VIOLATION_RESTRICTION, "Нарушение"),
                                       (BAN_RESTRICTION, "БАН"),
                                       (SHADOW_BAN_RESTRICTION, "ТЕНЕВОЙ БАН"),
                                   ],
                                   default=NO_RESTRICTION,)

    created_by = models.ForeignKey(
        "auth.User", verbose_name="Автор", on_delete=models.SET_NULL, null=True
    )

    def __str__(self) -> str:
        return f"{self.id} | {self.name} ({self.describtion[0:20]}...)"

    def get_absolute_url(self):
        return reverse('video', kwargs={'id': self.id})

    @property
    def likes(self):
        return self.like_set.filter(value＿gt=0).count()

    @property
    def dislikes(self):
        return self.like_set.filter(value＿lt=0).count()

    class Meta:
        verbose_name = "Видео-ролик",
        verbose_name_plural = "Видео-ролики"


class Like(models.Model):
    video_id = models.ForeignKey(Video, verbose_name="Видео",
                                 on_delete=models.SET_NULL,
                                 null=True)

    user_id = models.ForeignKey("auth.User",
                                on_delete=models.SET_NULL,
                                null=True)

    value = models.IntegerField(verbose_name="Лайк(1)/Дизлайк(-1)",
                                default=True)

    class Meta:
        verbose_name = "Лайк/Дизлайк",
        verbose_name_plural = "Лайки/Дизлайки"


class Comment(models.Model):
    text = models.TextField(verbose_name="Текст комментария")
    user = models.ManyToManyField("auth.User")
    video_id = models.ForeignKey(
        Video, on_delete=models.SET_NULL, null=True)
    created_datetime = models.DateTimeField(
        verbose_name="Время создания", auto_now=True)

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"
